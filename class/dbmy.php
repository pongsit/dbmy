<?php
namespace pongsit\dbmy;
	
class dbmy extends \mysqli{
	function __construct($hostname,$username,$password,$database){
		parent::__construct($hostname, $username, $password, $database);
		$this->set_charset("utf8");
	}
	
	function check(){
		$query = "SELECT * FROM INFORMATION_SCHEMA.TABLES 
					WHERE TABLE_SCHEMA = '".$GLOBALS['confVariables']['db']['database']."' 
					  AND TABLE_NAME = 'user';";
		$outputs = $this->query_array0($query);
		if(empty($outputs)){
			return 0;
		}
		return 1;
	}
	
	function insert($table,$array){
		if(empty($array) || empty($table)){return;}
		$column = '';
		$comma = '';
		$placeholder = '';
		foreach($array as $key=>$value){
			$column .= $comma.$key;
			$placeholder .= $comma."'".$this->escape_string($value)."'";
			$comma = ',';
		}
		$query = "INSERT INTO `$table` ($column) VALUES ($placeholder);";
		$result = $this->query($query);
		if($result){
			return $this->lastInsertRowID();
		}else{
			error_log('No inserted id in db insert');
			error_log($query);
			return false;
		}
	}
	
	function exec($query){
		$this->query($query);
	}
	
	function update($table,$array,$where){
		if(empty($array) || empty($table) || empty($where)){return;}
		$set = '';
		$comma = '';
		$placeholder = '';
		foreach($array as $key=>$value){
			$set .= $comma.$key."='".$this->escape_string($value)."'";
			$comma = ',';
		}
		$query = "UPDATE `$table` SET $set WHERE $where;";
		$statement = $this->prepare($query);
		
		$result = $statement->execute();
		return $result;
	}
	
	function delete($table,$where){
		if(empty($table) || empty($where)){return;}
		$query = "DELETE FROM `$table` WHERE $where;";
		$statement = $this->prepare($query);
		$result = $statement->execute();
		return $result;
	}
	
	function query_array($query){
		$result = $this->query($query);
		$rows = array();
		if(!empty($result)){
			while ($row = $result->fetch_assoc()) {
				$rows[] = $row;
			}
		}
		return $rows;
	}
	
	function query_array0($statement){
		$outputs = array();
		$query_inner = $this->query($statement);
		// fetch one time only
		if(!empty($query_inner)){
			$outputs = $query_inner->fetch_assoc();
		}
		return $outputs;
	}
	
	function get_table_column($table){
		$outputs = array();
    	$query = "desc `".$table."`;";
    	$_outputs = $this->query_array($query);
    	$outputs=array();
    	foreach($_outputs as $k=>$v){
    		$outputs[] = $v['Field'];
    	}
    	return $outputs;
    }
    
    function lastInsertRowID(){
    	return $this->insert_id;
    }
    
}