<?php

require_once("../system/init.php");

if($_SESSION['user']['id']!=1){
	echo 'คุณไม่มีสิทธิ์ใช้งานหน้านี้ครับ';
	error_log('Install ได้เฉพาะ Super Admin เท่านั้น');
	exit();
}

if($confVariables['db']['type'] != 'mysql'){
	echo 'หากต้องการใช้ MySql กรุณาปรับที่ conf.php';
	exit();
}

function scan_folder($folder_path){
	if(file_exists($folder_path)){
		// remove all the files starting with '.'
		$removed_hidden = array_filter(scandir($folder_path),function($item) {
				return !(substr($item,0,1) == '.');
		});
		// return with reindexed array, staring from 0
		return array_values($removed_hidden);
	}else{
		error_log($folder_path.' does not exist! See more detail in File class. ');
	}
}

// print_r(scan_folder('../'));
// Array ( [0] => auth [1] => bootstrap [2] => dbmy [3] => dbsq [4] => file [5] => index.php [6] => jquery [7] => line [8] => linebot [9] => linenotify [10] => model [11] => option [12] => password [13] => profile [14] => role [15] => site [16] => system [17] => time [18] => tmp [19] => url [20] => user [21] => view ) 

echo 'กรุณาใช้คำสั่ง ดังนี้<br><br>';
$query = '';
foreach(scan_folder('../') as $k=>$v){
	if(file_exists('../'.$v.'/install/dbmy.sql')){
		$query .= file_get_contents('../'.$v.'/install/dbmy.sql').'<br><br>';
	}
}

echo $query;
